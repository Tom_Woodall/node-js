// Import the 'http' module
const http = require('http');

// Create an HTTP server
const server = http.createServer((req, res) => {

  res.end('Hello http\n');
});

// Listen on port 3000
const port = 3000;
server.listen(port, () => {
  console.log(`Server is listening on port ${port}`);
});