const express = require('express');
const ejs = require('ejs');
const session = require('express-session');
const app = express();
const port = 3000;

app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/public'));
app.use(express.urlencoded({ extended: true }));

app.use(session({
    secret: 'secret-key',
    resave: false,
    saveUninitialized: true,
}));

const helloController = require('./controllers/helloController');
app.get('/', helloController.getHello);

app.get('/login', (req, res) => {
    res.render('hello', { username: req.session.username || 'Guest' });
});

app.post('/login', (req, res) => {
    const { username } = req.body;
    req.session.username = username; // Set the username in the session
    res.redirect('/');
});
  

app.get('/logout', (req, res) => {
    req.session.destroy(); // Destroy the session
    res.redirect('/');
});

app.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}`);
});