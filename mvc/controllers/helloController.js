// controllers/helloController.js
exports.getHello = (req, res) => {
    const username = req.session.username || 'Guest'; // Get the username from the session or use 'Guest'
    
    res.render('hello', { username });
  };
  